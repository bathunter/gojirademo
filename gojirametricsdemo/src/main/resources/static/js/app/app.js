'use strict'
var theApp = angular.module('sprint', ['ui.bootstrap', 'sprint.controllers',
    'sprint.services'
]);
theApp.constant("CONSTANTS", {
    getReport: "/sprint/"
});