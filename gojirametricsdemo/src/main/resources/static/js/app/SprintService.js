'use strict'
angular.module('sprint.services', []).factory('SprintService', ["$http", "CONSTANTS", function($http, CONSTANTS) {
    var service = {};

    service.getReport = function(sprintId) {
        var url = "boards/sprint/" + sprintId;
        return $http.get(url);
    };

    service.getStats = function () {
        return $http.get("boards/stats");
    };

    service.getSprints = function (boardId) {
        return $http.get("boards/sprints/get/"+boardId);
    };

    service.getSprintNames = function () {
        return $http.get("boards/sprint_names")
    };

    service.getBoards = function () {
        return $http.get("boards/get");
    };

    return service;
}]);