'use strict';
var module = angular.module('sprint.controllers', []);
module.controller("SprintRestController", ["$scope", "SprintService",
    function($scope, SprintService) {

    $scope.boardId = null;
    $scope.sprintId = null;
    $scope.fieldKeys = {
        keys : []
    };
        $scope.avgTimeFields = [];
        $scope.backFromColumnFields = [];
        $scope.isArray = function(obj) {
            return angular.isArray(obj);
        };

        $scope.getLink = function(obj) {
            try {
                if (obj.length == 0) {
                    return "#";
                } else {
                    return "https://ratesetter.atlassian.net/browse/" + obj[0] + "?jql=issueKey in (" + obj.join() + ")";
                }
            }catch(err) {
                console.log(err);
            }

        };
        $scope.visible = true;
        $scope.runSingleReport = function () {
            delete $scope.errorMessage;
            delete $scope.report;
            $scope.fieldKeys = {
                keys : []
            };
            $scope.avgTimeFields = [];
            $scope.backFromColumnFields = [];
            $scope.visible = false;
            $scope.reportLoading = true;

         SprintService.getReport($scope.selectedSprint.split(" ")[0]).then(function (value) {
             console.log(value);
             $scope.reports = [];
             $scope.report = value.data;
             $scope.boardNames = Object.keys(value.data);
             $scope.fieldKeys.keys = Object.keys(value.data[$scope.boardNames[0]]);
             $scope.fieldKeys.keys.forEach(function (key) {
                 $scope.fieldKeys[key] = $scope.report[$scope.boardNames[0]][key]['fieldName'];
             });
             $scope.boardNames.forEach(function (boardName) {
                 $scope.avgTimeFields.push($scope.report[boardName].averageTimeInColumn);
                 $scope.backFromColumnFields.push($scope.report[boardName].backFromColumn);
             });
             $scope.visible = true;
             delete $scope.reportLoading;
         }, function(reason) {
             $scope.errorMessage = reason.data.message;
             delete $scope.reportLoading;
         }, function(value) {
             console.log("no callback");
             delete $scope.reportLoading;
         });
        };

        $scope.runStatsReport = function () {
            delete $scope.errorMessage;
            $scope.visible = false;
            $scope.reportLoading = true;

            SprintService.getStats().then(function(value) {
                console.log(value.data);
                $scope.reports = [];
                value.data.forEach(function (report) {
                    $scope.reports.push(report);
                });
                $scope.visible = true;
                delete $scope.reportLoading;
            }, function(reason) {
                $scope.errorMessage = reason.data.message;
                delete $scope.reportLoading;
            }, function(value) {
                console.log("no callback");
                delete $scope.reportLoading;
            });
        };

        $scope.getFieldDescription = function (key) {
                return $scope.report[$scope.boardNames[0]][key].fieldDescription;
        };

        $scope.getNumberDescription = function (key, index) {
                return $scope.report[$scope.boardNames[index]][key].numberDescription;
        };

        $scope.getSprints = function () {
            delete $scope.selectedSprint;
            var boardId = angular.copy($scope.selectedBoard.id);
            SprintService.getSprintNames(boardId).then(function (data) {
                console.log(data.data);
                $scope.sprints = data.data;
                $scope.selectedSprint = $scope.sprints[0];
            });
        };

        $scope.getBoards = function () {
            SprintService.getBoards().then(function (data) {
                $scope.boards = data.data;
                $scope.selectedBoard = $scope.boards[0];
                if(!$scope.sprints){
                    $scope.getSprints();
                }
            });
        }

    }
]);