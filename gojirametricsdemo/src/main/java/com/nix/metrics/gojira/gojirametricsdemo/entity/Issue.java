package com.nix.metrics.gojira.gojirametricsdemo.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.nix.metrics.gojira.gojirametricsdemo.utils.Interval;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.nix.metrics.gojira.gojirametricsdemo.client.SprintReportParams.BUG;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Issue {

    @JsonProperty("id")
    private int id;
    @JsonProperty("key")
    private String key;
    @JsonProperty("fields")
    private Fields fields;
    @JsonProperty("changelog")
    private ChangeLog changelog;
    @JsonIgnore
    private Double estimateStatistic;
    @JsonIgnore
    private Double currentEstimateStatistic;
    @JsonIgnore
    private List<Interval> intervalList;
    @JsonIgnore
    private HashMap<String, List<Interval>> statuses;
    @JsonIgnore
    private HashMap<String, List<Interval>> intStatuses;
    @JsonIgnore
    private HashMap<String, Long> statusesMillis;

    public HashMap<String, List<Interval>> getIntStatuses() {
        return intStatuses;
    }

    public HashMap<String, List<Interval>> getStatuses() {

        return statuses;
    }

    public ChangeLog getChangelog() {
        return changelog;
    }

    public String getKey() {
        return key;
    }

    public void setIntervalList(Date startDate, Date endDate, String sprintName) {
        intervalList = new ArrayList<>();
        Interval interval = new Interval(startDate, endDate);
        this.getChangelog().getHistories(startDate, endDate).forEach(h -> {
            h.getFieldChanged("Sprint").ifPresent(i -> {
                if (isAdded(i, sprintName)) {
                    interval.setStartDate(h.getCreated());
                    intervalList.add(new Interval(interval.getStartDate(), interval.getEndDate()));
                } else {
                    interval.setEndDate(h.getCreated());
                }
            });
        });
        if ((interval.getStartDate().equals(startDate) && !interval.getEndDate().equals(endDate)
                || (interval.getStartDate().compareTo(interval.getEndDate()) > 0))) {
            intervalList.add(new Interval(startDate, interval.getEndDate()));
        }
        if (intervalList.size() == 0) {
            intervalList.add(new Interval(startDate, endDate));
        }
    }

    public List<Interval> getIntervalList() {
        return intervalList;
    }

    public String getLastSprint(String sprintList) {
        return sprintList.substring(sprintList.lastIndexOf(',') + 1).trim();
    }

    public boolean isAdded(Item item, String sprintName) {
        return getLastSprint(item.getToString()).equals(sprintName);
    }

    public boolean isDescriptionChanged(Date dateStart, Date dateEnd) {
        return this.getChangelog().getHistories(dateStart, dateEnd).stream()
                .anyMatch(h -> h.getFieldChanged("Description").isPresent());
    }

    public boolean isDescriptionChanged() {
        return this.getIntervalList().stream()
                .anyMatch(i -> isDescriptionChanged(i.getStartDate(), i.getEndDate()));
    }

    public boolean areStoryPointsChanged(Date dateStart, Date dateEnd) {
        return this.getChangelog().getHistories(dateStart, dateEnd).stream()
                .anyMatch(h -> h.getFieldChanged("Story Points").isPresent());
    }

    public boolean areStoryPointsChanged() {
        return this.getIntervalList().stream()
                .anyMatch(i -> areStoryPointsChanged(i.getStartDate(), i.getEndDate()));
    }

    public long getStoryPointsAdded(Date dateStart, Date dateEnd) {
        return this.getChangelog().getHistories(dateStart, dateEnd).stream()
                .flatMap(h -> h.getPointsAdded("Story Points").isPresent()
                        ? Stream.of(h.getPointsAdded("Story Points").getAsLong())
                        : Stream.empty())
                .reduce(0L,(s,n)->s+n);
    }

    public Double getStoryPointsChanged(Date dateStart, Date dateEnd) {
        return this.getChangelog().getHistories(dateStart, dateEnd).stream()
                .flatMap(h -> h.getPointsChanged("Story Points").isPresent()
                        ? Stream.of(h.getPointsChanged("Story Points").getAsDouble())
                        : Stream.empty())
                .reduce(0D,(s,n)->s+n);
    }

    public long getStoryPointsAdded() {
        return this.getIntervalList().stream()
                .map(i->getStoryPointsAdded(i.getStartDate(), i.getEndDate()))
                .reduce(0L,(s,n)->s+n);
    }

    public void setStatusChanges(Date dateStart, Date dateEnd) {
        statuses = new HashMap<>();

        this.getChangelog().getHistories(dateStart, dateEnd).forEach(h -> {
            h.getFieldChanged("Status").ifPresent(i -> {
                // From part means e're at the starting point, so...
                if (statuses.containsKey(i.getFromString())) {
                    statuses.get(i.getFromString()).add(new Interval(dateStart, h.getCreated()));
                } else {
                    //
                    statuses.put(i.getFromString(), new ArrayList<>(
                            Collections.singletonList(new Interval(dateStart, h.getCreated()))));
                }

                //To part means we're at the end, so...
                if (statuses.containsKey(i.getToString())) {
                    Interval currentInterval = statuses.get(i.getToString())
                            .get(statuses.get(i.getToString()).size() - 1);
                    if (currentInterval.getStartDate().equals(dateStart)) {
                        currentInterval.setStartDate(h.getCreated());
                    } else {
                        statuses.get(i.getToString()).add(new Interval(h.getCreated(), dateEnd));
                    }
                } else {
                    statuses.put(i.getToString(), new ArrayList<>(
                            Collections.singletonList(new Interval(h.getCreated(), dateEnd))));
                }
            });
        });
        //TODO pick the real status from earlieer history
        if (statuses.size() == 0) {
            statuses.put(getClosestStatus(dateStart), new ArrayList<>(
                    Collections.singletonList(new Interval(dateStart, dateEnd))));
        }
    }

    public String getClosestStatus(Date from){
        Optional<String> status = getChangelog().getHistories(from).stream()
                .filter(h->h.getFieldChanged("Status").isPresent())
                .max(Comparator.comparing(History::getCreated))
                .map(h->h.getFieldChanged("Status").get().getToString());
        return status.orElseGet(() -> this.getFields().getStatus().getName());
    }

    public List<Interval> setStatusesForIntervals(String status) {
        List<Interval> result = null;
        if (statuses.containsKey(status)) {
            List<Interval> combined = Stream.concat(statuses.get(status).stream(), intervalList.stream())
                    .sorted((a, b) -> a.getStartDate().compareTo(b.getStartDate()))
                    .collect(Collectors.toList());

            result = new ArrayList<>();
            Date currentEnd = combined.get(0).getEndDate();

            for (int i = 1; i < combined.size(); i++) {
                if (!combined.get(i).getStartDate().after(currentEnd)) {
                    if (combined.get(i).getEndDate().after(currentEnd)) {
                        if (!combined.get(i).getStartDate().equals(currentEnd)) {
                            result.add(new Interval(combined.get(i).getStartDate(), currentEnd));
                        }
                        currentEnd = combined.get(i).getEndDate();
                    } else {
                        result.add(new Interval(combined.get(i).getStartDate(), combined.get(i).getEndDate()));
                    }
                } else {
                    currentEnd = combined.get(i).getEndDate();
                }
            }
        }
        return result;
    }

    public void setIntStatuses() {
        intStatuses = new HashMap<>();
        statuses.forEach((k, v) -> intStatuses.put(k, setStatusesForIntervals(k)));
    }

    public HashMap<String, Long> getStatusesMillis() {
        return statusesMillis;
    }

    public void setStatusesMillis() {
        statusesMillis = new HashMap<>();
        intStatuses.forEach((k, v) -> statusesMillis.put(k,v.stream()
                .map(i -> i.getEndDate().getTime() - i.getStartDate().getTime())
                .mapToLong(Long::longValue).sum()));
    }

    public boolean wasInStatus(String status, Date date){
        return (intStatuses.containsKey(status))
                ? intStatuses.get(status).stream()
                .anyMatch(i->i.getStartDate().before(date) && i.getEndDate().after(date))
                : intStatuses.containsKey(status);
    }

    public Double getEstimateStatistic() {
        return estimateStatistic;
    }

    public void setEstimateStatistic(Double estimateStatistic) {
        this.estimateStatistic = estimateStatistic;
    }

    public Double getCurrentEstimateStatistic() {
        return currentEstimateStatistic;
    }

    public void setCurrentEstimateStatistic(Double currentEstimateStatistic) {
        this.currentEstimateStatistic = currentEstimateStatistic;
    }

    public Fields getFields() {
        return fields;
    }

    public String getType(){
        return getFields().getIssuetype().getName();
    }

    public boolean isNotABug(){
        return !getType().equals(BUG.get());
    }
}
