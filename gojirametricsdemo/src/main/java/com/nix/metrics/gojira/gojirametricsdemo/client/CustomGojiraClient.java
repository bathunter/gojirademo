package com.nix.metrics.gojira.gojirametricsdemo.client;

import static com.nix.gojira.restclient.client.Params.EXPAND;
import static com.nix.gojira.restclient.client.Params.JQL;
import static com.nix.gojira.restclient.client.Params.RVID;
import static com.nix.gojira.restclient.client.Params.SCRUM;
import static com.nix.gojira.restclient.client.Params.SID;
import static com.nix.metrics.gojira.gojirametricsdemo.client.PathParts.*;

import com.nix.metrics.gojira.gojirametricsdemo.entity.*;

import java.util.HashMap;
import java.util.List;
import javax.ws.rs.core.MediaType;

public class CustomGojiraClient extends BaseGojiraClient {

  public CustomGojiraClient(String username, String password, String jUrl) {
    super(username, password, jUrl);
  }

  public ChangeLogItems getChangeLogItems(String issueKey) {
    return getClient()
        .target(getJiraUri())
        .path(ISSUE.getPart())
        .path(issueKey)
        .path(CHANGELOG.getPart())
        .request(MediaType.APPLICATION_JSON)
        .get(ChangeLogItems.class);
  }
  public Velocities getVelocity(String boardID) {
    return getClient()
            .target(getJiraUri()+"/rest/greenhopper/1.0/rapid/charts/velocity?rapidViewId="+boardID)
            .request(MediaType.APPLICATION_JSON)
            .get(Velocities.class);

  }

  public Boards getScrumBoards() {
    return getClient()
        .target(getJiraUri())
        .path(AGILEBOARD.getPart())
        .queryParam("type", SCRUM.getParam())
        .request(MediaType.APPLICATION_JSON)
        .get(Boards.class);
  }


  public Sprints getBoardSprints(String boardID) {
    return getClient()
        .target(getJiraUri())
        .path(AGILEBOARD.getPart())
        .path(boardID)
        .path(SPRINT.getPart())
        .request(MediaType.APPLICATION_JSON)
        .get(Sprints.class);
  }

  public Issues getSprintIssues(String boardID, String sprintID) {
    return getClient()
        .target(getJiraUri())
        .path(AGILEBOARD.getPart())
        .path(boardID)
        .path(SPRINT.getPart())
        .path(sprintID)
        .path(AISSUE.getPart())
        .request(MediaType.APPLICATION_JSON)
        .get(Issues.class);
  }

  public Issues getSprintIssues(String boardID, String sprintID, String filter) {
    return getClient()
        .target(getJiraUri())
        .path(AGILEBOARD.getPart())
        .path(boardID)
        .path(SPRINT.getPart())
        .path(sprintID)
        .path(AISSUE.getPart())
        .queryParam("jql", filter)
        .request(MediaType.APPLICATION_JSON)
        .get(Issues.class);
  }

  public SprintReports getSprintReport(String boardID, String sprintID) {
    return getClient()
        .target(getJiraUri())
        .path(SPRINTREPORT.getPart())
        .queryParam(RVID.getParam(), boardID)
        .queryParam(SID.getParam(), sprintID)
        .request(MediaType.APPLICATION_JSON)
        .get(SprintReports.class);
  }

  public Issues getIssueSearchResult(List<String> keys) {
    return getClient()
        .target(getJiraUri())
        .path(SEARCH.getPart())
        .queryParam(EXPAND.getParam(), "changelog")
        .queryParam(JQL.getParam(), "issueKey in (" + String.join(",", keys) + ")")
        .request(MediaType.APPLICATION_JSON)
        .get(Issues.class);
  }

}
