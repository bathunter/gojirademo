package com.nix.metrics.gojira.gojirametricsdemo.exceptions;

public class ReportException extends Exception {

  public ReportException(String message) {
    super(message);
  }

  public ReportException(String message, Throwable cause) {
    super(message, cause);
  }
}
