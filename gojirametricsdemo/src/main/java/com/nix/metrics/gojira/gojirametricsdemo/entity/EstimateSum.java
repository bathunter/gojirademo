package com.nix.metrics.gojira.gojirametricsdemo.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class EstimateSum {
  @JsonProperty("value")
  private float sum;

  public float getSum() {
    return sum;
  }
}
