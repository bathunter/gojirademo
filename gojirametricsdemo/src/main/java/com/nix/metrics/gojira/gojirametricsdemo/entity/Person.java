package com.nix.metrics.gojira.gojirametricsdemo.entity;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Person {
  @JsonProperty("name")
  private String name;
  @JsonProperty("key")
  private String key;
  @JsonProperty("displayName")
  private String displayName;
  @JsonProperty("active")
  private boolean active;
  @JsonProperty("timeZone")
  private String timeZone;
}
