package com.nix.metrics.gojira.gojirametricsdemo.service;

import com.nix.metrics.gojira.gojirametricsdemo.client.CustomGojiraClient;
import com.nix.metrics.gojira.gojirametricsdemo.entity.*;
import com.nix.metrics.gojira.gojirametricsdemo.dto.Report;

import java.io.File;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

@Service
public class SprintServiceImpl implements SprintService {

    //  private final String username = "skorotkov@n-ix.com";
//  private final String password = "JovEC4ZDuwR37pqMIJEj49EC";
//  private final String jurl = "https://n-ix-nordic.atlassian.net";
    private final String username = "andriy.susyak@ratesetter.com";
    private final String password = "Unretentive.Pronunciation.159";
    private final String jurl = "https://ratesetter.atlassian.net";
    private final CustomGojiraClient customGojiraClient = new CustomGojiraClient(username, password, jurl);
    private final long DAY_IN_MILLIS = 86400000;
    private final long HOUR_IN_MILLIS = 3600000;


    @Override
    public Report getReport(String boardID, String id, String sprintName) {

        Report report = new Report(boardID, id);

        ResourceBundle bundle = ResourceBundle.getBundle("content-configuration");
        long start = System.currentTimeMillis();
        SprintReports sprintReports = customGojiraClient.getSprintReport(boardID, id);
        Sprint sprint = sprintReports.getSprint();
        SprintReport sprintReport = sprintReports.getSprintReport();
        Sprints sprints = customGojiraClient.getBoardSprints(boardID);
        List<String> resourceRowContent = Arrays.asList(ResourceBundle.getBundle("content-configuration").getString(sprintName).split(","));
        long sprintMidDateInterval = countCodeFreezeInterval(resourceRowContent.get(resourceRowContent.size() - 2));
        long sprintCodeFreezeInterval = countCodeFreezeInterval(resourceRowContent.get(resourceRowContent.size() - 1));
        Date sprintMidDate = new Date(sprint.getStartDate().getTime() + sprintMidDateInterval);
        Date sprintCodeFreezeDate = new Date(sprint.getStartDate().getTime() + sprintCodeFreezeInterval);
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd-HH:mm");
        DecimalFormat decimalFormat = new DecimalFormat("0.00");
        Date sprintStartDueDate = null;
        try {
            sprintStartDueDate = dateFormat.parse(Arrays.asList(bundle.getString(sprintName).split(",")).get(resourceRowContent.size()-3));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        double sprintStartDelay = (sprint.getStartDate().getTime() - sprintStartDueDate.getTime()) / HOUR_IN_MILLIS;
        report.setStartDelay(TableRow.create(sprintStartDelay, "Sprint start delay in Jira, hrs", "Number of hours between the Sprint start due date and actual sprint start", "Sprint start due date = " + dateFormat.format(sprintStartDueDate)+",\n" +
                "Actual sprint start = "+dateFormat.format(sprint.getStartDate()) +",\n" +
                "Result = "+ sprintStartDelay+" hrs\n"));

        start = System.currentTimeMillis();
        Issues issues = customGojiraClient.getIssueSearchResult(sprintReport.getAllIssueKeys());
        List<HashMap<String, Object>> issuesMap = sprintReport.getAllIssuesMapList();
        System.out.println(String.format("Issue report time elapsed: %d", start - System.currentTimeMillis()));
        Date endDate = (sprint.getEndDate().before(new Date())) ? sprint.getEndDate() : new Date();


        issues.getIssues().forEach(i -> i.setIntervalList(sprint.getStartDate(),
                endDate, sprint.getName()));
        issues.getIssues().forEach(i -> i.setStatusChanges(sprint.getStartDate(), endDate));
        issues.getIssues().forEach(Issue::setIntStatuses);
        issues.getIssues().forEach(Issue::setStatusesMillis);
        issues.getIssues().forEach(i -> {
            issuesMap.stream().filter(m -> m.get("key").equals(i.getKey())).findFirst().ifPresent(e -> {
                i.setEstimateStatistic((e.get("estimateStatistic") != null) ? (Double) e.get("estimateStatistic") : null);
                i.setCurrentEstimateStatistic((e.get("currentEstimateStatistic") != null) ? (Double) e.get("currentEstimateStatistic") : null);
            });
        });

        List<String> unestimatedIssuesPerStart = issues.getIssues().stream()
                .filter(i -> i.isNotABug() && (i.getEstimateStatistic() == null || i.getEstimateStatistic().equals(0D)))
                .map(Issue::getKey)
                .collect(Collectors.toList());

        double storyPointsEstimated = issues.getStoryPointsBeforeDate(sprint.getStartDate(), issues.getIssues());
        double storyPointsInSprint = issues.getStoryPointsSum();

        Velocity velocity = customGojiraClient.getVelocity(boardID).getVelocities().get(sprint.getId());
        double actualVelocity = velocity.getCompleted().get("value");
        double estimatedVelocity = velocity.getEstimated().get("value");
        report.setVelocity(TableRow.create(sprintReport.getVelocity(), "Actual to planned velocity", "Planned - the number of sprint story points at a time sprint was started",
                "Planned story points = "+ estimatedVelocity + ", \n" +
                "Actual story points = " + actualVelocity + ", \n" +
                "Result = " + decimalFormat.format(estimatedVelocity/actualVelocity) + "\n"));


        report.setUnestimatedIssuesPerStart(TableRow.create(unestimatedIssuesPerStart, "Number of unestimated tickets",
                "Number of tickets without a story points estimate as of sprint start",
                "Number of unestimated tickets = " + unestimatedIssuesPerStart.size() + ",\n" +
                "Number of ALL tickets = " + issues.getIssues().size() + ", \n" +
                "% of unestimated = "+decimalFormat.format((double)unestimatedIssuesPerStart.size() / issues.getIssues().size() * 100)  + "\n"));

        long numberOfIssuesAddedDuringSprint = sprintReport.getNonBugIssuesAddedDuringSprint(issues.getIssues());
        report.setIssuesAddedDuringSprint(TableRow.create(numberOfIssuesAddedDuringSprint, "Number of added tickets", "Number of non-bug tickets added during the active sprint",
                "Total number of added tickets = "+numberOfIssuesAddedDuringSprint + ",\n" +
                "Number of added story points = " + storyPointsInSprint + ",\n" +
                "Number of story points in sprint = "+ storyPointsEstimated + ",\n" +
                "% of story points added = "+ decimalFormat.format((storyPointsInSprint / storyPointsEstimated) * 100) + ", \n"+
                "Number of bugs, defects, sub-bugs and sub-defects added = "+ sprintReport.getBugIssuesAddedDuringSprint(issues.getIssues()) + "\n"));

        List<Issue> issuesWithChangedDescription = issues.getIssuesWithChangedDescription();
        double numberOfChangedStoryPoints = issues.getStoryPointsSumBetweenDates(sprint.getStartDate(), sprint.getEndDate(), issuesWithChangedDescription);
        report.setIssuesChangedDuringSprint(TableRow.create(issuesWithChangedDescription, "Number of changed tickets", "Number of tickets having description changed any time during the active sprint",
                "Number of changed tickets = "+issuesWithChangedDescription.size() + ",\n" +
                "Number of changed story points = "+numberOfChangedStoryPoints+",\n" +
                "Number of story points in sprint = " + storyPointsEstimated + ",\n" +
                "% of sprint story points changed = " + decimalFormat.format(numberOfChangedStoryPoints / storyPointsEstimated * 100) + " \n"));

        List<Issue> issuesClosedByMidSprint = issues.getClosedIssuesBeforeDate(sprintMidDate);
        double closedStoryPointsByMidSprint = issues.getStoryPointsBeforeDate(sprintMidDate, issuesClosedByMidSprint);
        double storyPointsForWholeSprint = issues.getStoryPointsSum();
        double midSprintProgressValue = (closedStoryPointsByMidSprint / storyPointsForWholeSprint) * 100;
        if (Double.isNaN(midSprintProgressValue)){
            midSprintProgressValue = 0;
        }
        report.setMidSprintProgress(TableRow.create(midSprintProgressValue, "Mid sprint progress, %", "Percentage of sprint planned story points closed by mid sprint",
                "Number of story points closed by mid sprint  = "+ closedStoryPointsByMidSprint + ",\n" +
                "Number of tickets closed by mid sprint = "+getIssueKeys(issuesClosedByMidSprint).size()+",\n" +
                "Number of story points in sprint = "+storyPointsForWholeSprint+",\n" +
                "Result = " + decimalFormat.format((closedStoryPointsByMidSprint/storyPointsForWholeSprint) * 100) +"% \n"));


        List<Issue> issuesClosedByCodeFreeze = issues.getClosedIssuesBeforeDate(sprintCodeFreezeDate);
        double closedStoryPointsByCodeFreeze = issues.getStoryPointsBeforeDate(sprintCodeFreezeDate, issuesClosedByCodeFreeze);
        double codeFreezeProgressValue = (closedStoryPointsByCodeFreeze / storyPointsForWholeSprint) * 100;
        if (Double.isNaN(codeFreezeProgressValue)){
            codeFreezeProgressValue = 0;
        }
        report.setCodeFreezeProgress(TableRow.create(codeFreezeProgressValue, "Code freeze progress, %", "Percentage of sprint planned story points closed by code freeze",
                "Number of story points closed by code freeze  = " + closedStoryPointsByCodeFreeze + ",\n" +
                        "Number of tickets closed by code freeze = " + getIssueKeys(issuesClosedByCodeFreeze).size() + ",\n" +
                        "Number of story points in sprint = " + storyPointsForWholeSprint + ",\n" +
                        "Result = "+ decimalFormat.format((closedStoryPointsByCodeFreeze / storyPointsForWholeSprint) * 100) + "% \n"));

        List<TableRow> avgTimeInColumns = new ArrayList<>();
        List<TableRow> backList = new ArrayList<>();
        List<String> columnsAvg = Arrays.asList(bundle.getString("columnsAvg").split(","));
        List<String> columnsBack = Arrays.asList(bundle.getString("columnsBack").split(","));
        for (String column : columnsAvg) {
            try {
                avgTimeInColumns.add(TableRow.create(issues.getAvgTimeForStatus(column) / HOUR_IN_MILLIS, "Average time in column " + column + ", hrs", "", ""));
            } catch (ArithmeticException e) {
                avgTimeInColumns.add(TableRow.create(0, "Average time in column " + column + ", hrs", "", ""));
            }
        }

        for (String column : columnsBack) {
            backList.add(TableRow.create(issues.getIssuesBackFromColumn(column).size(), "Tickets back from column " + column + " X", "", ""));
        }


        report.setAverageTimeInColumn(avgTimeInColumns);
        report.setBackFromColumn(backList);

        List<Issue> issuesCreatedTwoWeeksBeforeSprintStart = issues.getIssuesCreatedBeforeDate(new Date(sprint.getStartDate().getTime() - (DAY_IN_MILLIS * 14)));
        double storyPointsCreatedTwoWeeksBeforeSprintStart = issues.getStoryPointsSum(issuesCreatedTwoWeeksBeforeSprintStart);
        double requirementsMaturityFactor = (storyPointsCreatedTwoWeeksBeforeSprintStart/storyPointsInSprint) * 100;
        report.setRequirementsMaturityFactor(TableRow.create(requirementsMaturityFactor, "Requirements maturity factor, %", "Percentage of the sprint with requirements created at least 2 weeks before sprint start",
                "Number of story points created at least 2 weeks before the sprint start = " + storyPointsCreatedTwoWeeksBeforeSprintStart + ",\n" +
                "Number of tickets created at least 2 weeks before the sprint start = "+issuesCreatedTwoWeeksBeforeSprintStart.size() + ",\n" +
                "Number of story points in sprint = "+storyPointsInSprint + ",\n" +
                "Result = "+ decimalFormat.format(storyPointsCreatedTwoWeeksBeforeSprintStart / storyPointsInSprint * 100)+ "%\n"));

//        List<Sprint> futureSprints = sprints.getFutureSprints();
//        Map sprintReadinessFactor = generateSprintReadinessFactor(sprints, boardID);
//        report.setNextSprintsReadinessFactor(TableRow.create(sprintReadinessFactor.get("readinessFactor"), "Next sprints readiness factor", "Number of future sprints work ready for development",
//                "Next "+ futureSprints.size() + " sprints total estimated story points = " + sprintReadinessFactor.get("nextSprintsEstimatedStoryPoints") +",\n" +
//                "Last " + sprints.getThreeLastSprints().size() + " sprints total story points closed = "+sprintReadinessFactor.get("lastSprintsStoryPointsTotal")+"\n"));

        return report;
    }

    @Override
    public List<String> getSprintNames() {
        ResourceBundle bundle = ResourceBundle.getBundle("content-configuration");
        List<String> sprints = new ArrayList<>();
        Enumeration<String> enumeration = bundle.getKeys();
        while (enumeration.hasMoreElements()) {
            String element = enumeration.nextElement();
            if (element.startsWith("s", 0)) {
                sprints.add(element);
            }
        }
        return sprints;
    }

    @Override
    public Map<String, Report> generateReport(String sprintName) {

        List<String> boardIds = Arrays.asList(ResourceBundle.getBundle("content-configuration").getString("boardIds").split(","));
        List<String> boardNames = Arrays.asList(ResourceBundle.getBundle("content-configuration").getString("boardNames").split(","));
        List<String> resourceRowContent = Arrays.asList(ResourceBundle.getBundle("content-configuration").getString(sprintName).split(","));
        Map<String, Report> result = new HashMap<>();
        for (int i = 0; i < boardIds.size(); i++) {
            result.put(boardNames.get(i), getReport(boardIds.get(i), resourceRowContent.get(i), sprintName));
        }

        return result;
    }

    @Override
    public File getReport() {
        return null;
    }

    @Override
    public List<Board> getBoards() {
        return customGojiraClient.getScrumBoards().getBoards();
    }

    @Override
    public List<Sprint> getSprints(String boardId) {
        return customGojiraClient.getBoardSprints(boardId).getSprints().stream()
                .filter(s -> !s.getState().equals("future")).collect(Collectors.toList());
    }

    private long countCodeFreezeInterval(String interval) {
        String[] codeFreezeInterval = interval.split(":");
        return (Long.parseLong(codeFreezeInterval[0].replace("d", "")) * DAY_IN_MILLIS) + (Long.parseLong(codeFreezeInterval[1].replace("h", "")) * HOUR_IN_MILLIS);
    }

    private Map generateSprintReadinessFactor(Sprints sprints, String boardId) {
        List<Sprint> futureSprints = sprints.getFutureSprints();
        List<Sprint> previousSprints = sprints.getThreeLastSprints();
        Map<String, Object> result = new HashMap<>();
        if (CollectionUtils.isEmpty(futureSprints) || CollectionUtils.isEmpty(previousSprints)) {
            result.put("readinessFactor", 0);
            return result;
        }
        float previousSprintsVelocity = 0;
        double storyPointsClosedByPreviousSprints = 0;
        for (Sprint previousSprint : previousSprints) {
            SprintReports sprintReports = customGojiraClient.getSprintReport(boardId, previousSprint.getId());
            Date endDate = (previousSprint.getEndDate().before(new Date())) ? previousSprint.getEndDate() : new Date();

            previousSprintsVelocity += sprintReports.getSprintReport().getVelocity();
            Issues issues = customGojiraClient.getIssueSearchResult(sprintReports.getSprintReport().getAllIssueKeys());
            issues.getIssues().forEach(i -> i.setIntervalList(previousSprint.getStartDate(),
                    endDate, previousSprint.getName()));
            storyPointsClosedByPreviousSprints += issues.getStoryPointsSum(issues.getClosedIssuesBeforeDate(new Date()));
        }

        result.put("lastSprintsStoryPointsTotal", storyPointsClosedByPreviousSprints);
        previousSprintsVelocity = previousSprintsVelocity / previousSprints.size();

        float futureSprintsStoryPointsAverage = 0;
        double storyPointsEstimatedForNextSprints = 0;
        SprintReports sprintReports = null;
        for (int sprintIndex = 0; sprintIndex < futureSprints.size(); sprintIndex++) {
            try {
                sprintReports = customGojiraClient.getSprintReport(boardId, futureSprints.get(sprintIndex).getId());
            }catch (Exception e){
                break;
            }
            Sprint sprint = sprintReports.getSprint();
            Issues issues = customGojiraClient.getIssueSearchResult(sprintReports.getSprintReport().getAllIssueKeys());

            Date endDate = (sprint.getEndDate().before(new Date())) ? sprint.getEndDate() : new Date();

            issues.getIssues().forEach(i -> i.setIntervalList(sprint.getStartDate(),
                    endDate, sprint.getName()));
            storyPointsEstimatedForNextSprints += issues.getStoryPointsBeforeDate(sprint.getStartDate(), issues.getIssues());
            futureSprintsStoryPointsAverage += issues.getStoryPointsAddedSum();
        }

        result.put("nextSprintsEstimatedStoryPoints", storyPointsEstimatedForNextSprints);
        futureSprintsStoryPointsAverage = futureSprintsStoryPointsAverage / futureSprints.size();

        try {
            result.put("readinessFactor",futureSprintsStoryPointsAverage / previousSprintsVelocity);
        } catch (ArithmeticException e) {
            result.put("readinessFactor", 0);
        }
        return result;
    }

    private List<String> getIssueKeys(List<Issue> issues){
        List<String> result = new ArrayList<>();
        for(Issue issue : issues){
            result.add(issue.getKey());
        }
        return result;
    }
}
