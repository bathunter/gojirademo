package com.nix.metrics.gojira.gojirametricsdemo.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.nix.metrics.gojira.gojirametricsdemo.client.SprintReportParams.*;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SprintReport {

    @JsonProperty("completedIssuesInitialEstimateSum")
    private EstimateSum completedIssuesInitialEstimateSum;
    @JsonProperty("completedIssuesEstimateSum")
    private EstimateSum completedIssuesEstimateSum;
    @JsonProperty("issuesNotCompletedInitialEstimateSum")
    private EstimateSum issuesNotCompletedInitialEstimateSum;
    @JsonProperty("issuesNotCompletedEstimateSum")
    private EstimateSum issuesNotCompletedEstimateSum;
    @JsonProperty("allIssuesEstimateSum")
    private EstimateSum allIssuesEstimateSum;
    @JsonProperty("puntedIssuesInitialEstimateSum")
    private EstimateSum puntedIssuesInitialEstimateSum;
    @JsonProperty("puntedIssuesEstimateSum")
    private EstimateSum puntedIssuesEstimateSum;
    @JsonProperty("issuesCompletedInAnotherSprintInitialEstimateSum")
    private EstimateSum issuesCompletedInAnotherSprintInitialEstimateSum;
    @JsonProperty("issuesCompletedInAnotherSprintEstimateSum")
    private EstimateSum issuesCompletedInAnotherSprintEstimateSum;
    @JsonProperty("issueKeysAddedDuringSprint")
    private LinkedHashMap<String, Object> issueKeysAddedDuringSprint;
    @JsonProperty("puntedIssues")
    private List<LinkedHashMap<String, Object>> puntedIssues;
    @JsonProperty("issuesNotCompletedInCurrentSprint")
    private List<LinkedHashMap<String, Object>> issuesNotCompletedInCurrentSprint;
    @JsonProperty("issuesCompletedInAnotherSprint")
    private List<LinkedHashMap<String, Object>> issuesCompletedInAnotherSprint;
    @JsonProperty("completedIssues")
    private List<LinkedHashMap<String, Object>> completedIssues;

    public EstimateSum getCompletedIssuesInitialEstimateSum() {
        return completedIssuesInitialEstimateSum;
    }

    public EstimateSum getCompletedIssuesEstimateSum() {
        return completedIssuesEstimateSum;
    }

    public EstimateSum getIssuesNotCompletedInitialEstimateSum() {
        return issuesNotCompletedInitialEstimateSum;
    }

    public EstimateSum getIssuesNotCompletedEstimateSum() {
        return issuesNotCompletedEstimateSum;
    }

    public EstimateSum getPuntedIssuesInitialEstimateSum() {
        return puntedIssuesInitialEstimateSum;
    }

    public EstimateSum getPuntedIssuesEstimateSum() {
        return puntedIssuesEstimateSum;
    }

    public EstimateSum getIssuesCompletedInAnotherSprintInitialEstimateSum() {
        return issuesCompletedInAnotherSprintInitialEstimateSum;
    }

    public EstimateSum getIssuesCompletedInAnotherSprintEstimateSum() {
        return issuesCompletedInAnotherSprintEstimateSum;
    }

    public LinkedHashMap getIssueKeysAddedDuringSprint() {
        return issueKeysAddedDuringSprint;
    }

    public int getIssuesAddedDuringSprint() {
        return issueKeysAddedDuringSprint.size();
    }

    public List<String> getIssueKeysListAddedDuringSprint() {
        return issueKeysAddedDuringSprint
                .entrySet()
                .stream()
                .map(Map.Entry::getKey)
                .collect(Collectors.toList());
    }

    public int getIssuesRemovedDuringSprint() {
        return puntedIssues.size();
    }

    public long getNonBugIssuesAddedDuringSprint(List<Issue> issues) {
        return issues.stream().filter(i -> isNonbugIssue(i)).count();
//                issueKeysAddedDuringSprint
//                        .entrySet()
//                        .stream()
//                        .filter(c -> isNonbugIssue(c.getKey()))
//                        .count();
    }

    public long getBugIssuesAddedDuringSprint(List<Issue> issues) {
        return issues.stream().filter(i -> !isNonbugIssue(i)).count();
    }

    private boolean isNonbugIssue(Issue issue) {
        return !issue.getType().equalsIgnoreCase("bug");
    }

    public float getIssuesInitialEstimateSum() {
        return getCompletedIssuesInitialEstimateSum().getSum()
                + getIssuesCompletedInAnotherSprintInitialEstimateSum().getSum()
                + getIssuesNotCompletedInitialEstimateSum().getSum()
                + getPuntedIssuesInitialEstimateSum().getSum();
    }

    public float getIssuesEstimateSum() {
        return getCompletedIssuesEstimateSum().getSum()
                + getIssuesCompletedInAnotherSprintEstimateSum().getSum()
                + getIssuesNotCompletedEstimateSum().getSum()
                + getPuntedIssuesEstimateSum().getSum();
    }

    public float getVelocity() {
        return (getIssuesInitialEstimateSum() != 0)? getIssuesEstimateSum() / getIssuesInitialEstimateSum() : 0;
    }

    private List<LinkedHashMap<String, Object>> getAllIssueList() {
        return Stream.concat(Stream.concat(completedIssues.stream(),
                issuesNotCompletedInCurrentSprint.stream()),
                Stream.concat(issuesCompletedInAnotherSprint.stream(),
                        puntedIssues.stream()))
                .collect(Collectors.toList());
    }

    public List<String> getAllIssueKeys() {
        return getAllIssueList().stream()
                .map(c -> c.get(KEY.get()).toString())
                .collect(Collectors.toList());
    }

    public List<HashMap<String, Object>> getAllIssuesMapList() {

        List<HashMap<String, Object>> issues = new ArrayList<>();

        getAllIssueList().forEach(i -> {
            HashMap<String, Object> issue = new HashMap<>();
            issue.put(KEY.get(), i.get(KEY.get()).toString());
            issue.put(ESTAT.get(), getSum(i.get(ESTAT.get())));
            issue.put(CESTAT.get(), getSum(i.get(CESTAT.get())));
            issues.add(issue);
        });

        return issues;
    }

    private Double getSum(Object stat) {
        if (stat instanceof Map) {
            if (((Map) stat).get(SVALUE.get()) != null && ((Map) stat).get(SVALUE.get()) instanceof Map) {
                return (((Map) ((Map) stat).get(SVALUE.get())).get(VALUE.get()) != null)
                        ? Double.parseDouble(((Map) ((Map) stat).get(SVALUE.get())).get(VALUE.get()).toString())
                        : null;
            }
        }
        return null;
    }

}
