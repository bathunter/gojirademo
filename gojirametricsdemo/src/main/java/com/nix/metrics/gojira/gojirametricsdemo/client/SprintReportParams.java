package com.nix.metrics.gojira.gojirametricsdemo.client;

public enum SprintReportParams {
  KEY("key"),
  TYPENAME("typeName"),
  BUG("Bug"),
  ESTAT("estimateStatistic"),
  CESTAT("currentEstimateStatistic"),
  VALUE("value"),
  SVALUE("statFieldValue");

  private String parm;

  SprintReportParams(String parm) {
    this.parm = parm;
  }

  public String get() {
    return parm;
  }
}
