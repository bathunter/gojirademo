package com.nix.metrics.gojira.gojirametricsdemo.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.nix.metrics.gojira.gojirametricsdemo.deserializer.MultiDateDeserializer;

import java.util.*;

@JsonIgnoreProperties(ignoreUnknown = true)
public class History {
  @JsonProperty("id")
  private int id;
  @JsonProperty("author")
  private Person author;
  @JsonProperty("created") @JsonDeserialize(using = MultiDateDeserializer.class)
//  @JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd'T'HH:mm:ss.SSSz", timezone="EEST")
  private Date created;
  @JsonProperty("items")
  private List<Item> items;

  public List<Item> getItems() {
    return items;
  }

  public Date getCreated() {
    return created;
  }

  public Optional<Item> getFieldChanged(String field){
    return items.stream().filter(i -> !i.getField().toLowerCase().isEmpty() && i.getField().toLowerCase().equals(field.toLowerCase())).findFirst();
  }

  public OptionalLong getPointsAdded(String field){
    return items.stream().filter(i -> i.getField().toLowerCase().equals(field.toLowerCase()))
            .map(i-> calcAdded(i.getFromString(), i.getToString()))
            .filter(s->s>0)
            .mapToLong(Long::longValue)
            .findFirst();
  }

  public OptionalDouble getPointsChanged(String field){
    return items.stream().filter(i -> i.getField().toLowerCase().equals(field.toLowerCase()))
            .map(i-> calcAdded(i.getFromString(), i.getToString()))
            .mapToDouble(Long::longValue)
            .findFirst();
  }

  private long calcAdded(String before, String after){
    return Long.parseLong((after != null && !after.isEmpty())?after:"0",10)
            - Long.parseLong((before != null && !before.isEmpty())?before:"0",10);
  }

}
