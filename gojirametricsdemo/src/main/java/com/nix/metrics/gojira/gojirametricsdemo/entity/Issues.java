package com.nix.metrics.gojira.gojirametricsdemo.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.*;
import java.util.stream.Collectors;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Issues {
  @JsonProperty("issues")
  private List<Issue> issues;

  private List<String> statusesOrder = Arrays.asList("Open", "In Development", "Initial PM Review", "Code Review", "QA Review", "PM Review", "Done");
  public List<Issue> getIssues() {
    return issues;
  }

  public List<Issue> getIssuesDescriptionChanged(){
    return issues.stream()
          .filter(i -> i.getChangelog().getHistories().stream().anyMatch(h -> h.getFieldChanged("Description").isPresent()))
          .collect(Collectors.toList());
  }

  public List<Issue> getIssuesSprintChanged(){
    return issues.stream()
        .filter(i -> i.getChangelog().getHistories().stream().anyMatch(h -> h.getFieldChanged("Sprint").isPresent()))
        .collect(Collectors.toList());

  }

  public List<Issue> getIssuesStoryPointsChanged(){
    return issues.stream()
        .filter(i -> i.getChangelog().getHistories().stream().anyMatch(h -> h.getFieldChanged("Story Points").isPresent()))
        .collect(Collectors.toList());
  }

  public List<Issue> getIssuesDescriptionChanged(Date dateStart, Date dateEnd){
    return issues.stream()
        .filter(i -> i.getChangelog().getHistories(dateStart, dateEnd).stream().anyMatch(h -> h.getFieldChanged("Description").isPresent()))
        .collect(Collectors.toList());
  }

  public List<Issue> getIssuesSprintChanged(Date dateStart, Date dateEnd){
    return issues.stream()
        .filter(i -> i.getChangelog().getHistories(dateStart, dateEnd).stream().anyMatch(h -> h.getFieldChanged("Sprint").isPresent()))
        .collect(Collectors.toList());

  }

  public List<Issue> getIssuesStoryPointsChanged(Date dateStart, Date dateEnd){
    return issues.stream()
        .filter(i -> i.getChangelog().getHistories(dateStart, dateEnd).stream().anyMatch(h -> h.getFieldChanged("Story Points").isPresent()))
        .collect(Collectors.toList());
  }

  public float getdescriptionChangedStat(){
  //  int total = issues.size();
   // long sp = issues.stream().filter(Issue::isDescriptionChanged).count();

    return (float)issues.stream().filter(Issue::isDescriptionChanged).count()/issues.size();
  }

  public float getStoryPointsChangedStat(){
    //  int total = issues.size();
    //  long sp = issues.stream().filter(Issue::areStoryPointsChanged).count();

    return (float)issues.stream().filter(Issue::areStoryPointsChanged).count()/issues.size();
  }

  public List<Issue> getIssuesWithChangedDescription(){
    return issues.stream().filter(Issue::isDescriptionChanged).collect(Collectors.toList());
  }

  public long getStoryPointsAddedSum(){
    //  int total = issues.size();
    //  long sp = issues.stream().filter(Issue::areStoryPointsChanged).count();

    return issues.stream().map(Issue::getStoryPointsAdded).mapToLong(Long::longValue).sum();
  }

  public long getStoryPointsAddedCount(){
    return issues.stream().map(Issue::getStoryPointsAdded).mapToLong(Long::longValue).count();
  }


/*  public HashSet<String> getIssuesStatuses(Date dateStart, Date dateEnd){
    HashSet<String> stSet = new HashSet<>();
    issues
        .forEach(i -> i.getChangelog().getHistories(dateStart, dateEnd)
            .forEach(h -> {
              h.getFieldChanged("Status").ifPresent(item -> {
                stSet.add(item.getToString());
                stSet.add(item.getFromString());
              });

            }));
    return stSet;
  }*/

  public HashSet<String> getIssuesStatuses(Date dateStart, Date dateEnd){
    HashSet<String> stSet = new HashSet<>();
    issues.forEach(i->i.getStatuses().forEach((k,v)->{stSet.add(k);}));
    return stSet;
  }

  public HashMap<String, Long> getStatusTimeStat(Date dateStart, Date dateEnd){
    HashMap<String, Long> timeStat = new HashMap<>();
    HashSet<String> stSet = getIssuesStatuses(dateStart, dateEnd);
    for(String status : stSet){
      timeStat.put(status, getAvgTimeForStatus(status));
    }
    return timeStat;
  }

  public long getAvgTimeForStatus(String status){
    List<Issue> issueList = issues.stream()
        .filter(i -> i.getStatusesMillis().containsKey(status)).collect(Collectors.toList());
    return issueList.stream()
        .map(i -> i.getStatusesMillis().get(status))
        .mapToLong(Long::longValue).sum()/issueList.size();

  }

  public List<Issue> getIssuesBackFromColumn(String status){
    List<Issue> result = new ArrayList<>();
    List<History> histories;

    int statusIndex = statusesOrder.indexOf(status);
    for(Issue issue : issues){
      histories = issue.getChangelog().getHistories();
      for(History history : histories){
        for(Item item : history.getItems()){
          if(item.getField().equalsIgnoreCase("status")){
            if(item.getFromString().equalsIgnoreCase(status) && statusesOrder.indexOf(item.getToString()) < statusIndex){
              if(!result.contains(issue)) {
                result.add(issue);
              }
            }
          }
        }
      }
    }
    return result;
  }

  public List<Issue> getClosedIssuesBeforeDate(Date toDate){
    List<Issue> result = new ArrayList<>();
    List<History> histories;
    for(Issue issue : issues){
      histories = issue.getChangelog().getHistories(toDate);
      for(History history : histories){
        for(Item item : history.getItems()){
          if(item.getField().equalsIgnoreCase("status") && item.getToString().equalsIgnoreCase("Done")){
            if(!result.contains(issue)){
              result.add(issue);
            }
          }
        }
      }
    }
    return result;
  }

  public Double getStoryPointsBeforeDate(Date toDate, List<Issue> issues){
    Double result = 0.0;
    List<History> histories;
    for(Issue issue : issues){
      histories = issue.getChangelog().getHistories(toDate);
      result += fulfilStoryPointsListResult(histories);
    }
    return result;
  }

  public Double getStoryPointsSum(){
    Double result = 0.0;
    List<History> histories;
    for(Issue issue : issues){
      histories = issue.getChangelog().getHistories();
      result += fulfilStoryPointsListResult(histories);
    }
    return result;
  }

  public Double getStoryPointsSum(List<Issue> issues) {
    Double result = 0.0;
    List<History> histories;
    for(Issue issue : issues){
      histories = issue.getChangelog().getHistories();
      result += fulfilStoryPointsListResult(histories);
    }
    return result;
  }

  public Double getStoryPointsSumBetweenDates(Date fromDate, Date toDate, List<Issue> issues){
    Double result = 0.0;
    List<History> histories;
    for(Issue issue : issues){
      histories = issue.getChangelog().getHistories(fromDate, toDate);
      result += fulfilStoryPointsListResult(histories);
    }
    return result;
  }


  public List<Issue> getIssuesCreatedBeforeDate(Date toDate){
    List<Issue> result = new ArrayList<>();
    for(Issue issue : issues){
      if(issue.getFields().getCreated().getTime() < toDate.getTime()){
        result.add(issue);
      }
    }
    return result;
  }





  private double fulfilStoryPointsListResult (List<History> histories){
    double res = 0.0;
    for(History history : histories){
      for(Item item : history.getItems()){
        if(item.getField().equalsIgnoreCase("Story Points")){
          if(item.getFromString() != null && !item.getFromString().equals("")){
            res -= Double.parseDouble(item.getFromString());
          }
          if(item.getToString() != null && !item.getToString().equals("")) {
            res += Double.parseDouble(item.getToString());
          }
        }
      }
    }
    return res;
  }
  public Double getPointsCommitedByDate(Date from, Date to){
    return issues.stream()
            .filter(i->i.isNotABug() && i.wasInStatus("Done", to))
            .map(i-> toDouble(i.getEstimateStatistic()) + i.getStoryPointsChanged(from, to))
            .reduce(0D,(s,n)->s+n);
  }

  private Double toDouble(Double d){
    return (d == null)? 0D: d;
  }

}
