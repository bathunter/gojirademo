package com.nix.gojira.restclient.client;

public enum Params {

  SCRUM("scrum"),
  RVID("rapidViewId"),
  MAXRESULTS("maxResults"),
  SID("sprintId"),
  EXPAND("expand"),
  JQL("jql");

  public String getParam() {
    return param;
  }

  private String param;

  Params(String param) {
    this.param = param;
  }
}
