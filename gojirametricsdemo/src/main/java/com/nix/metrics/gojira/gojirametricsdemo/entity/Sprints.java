package com.nix.metrics.gojira.gojirametricsdemo.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


@JsonIgnoreProperties(ignoreUnknown = true)
public class Sprints {
  @JsonProperty("values")
  private List<Sprint> sprints;

  public List<Sprint> getSprints() {
    return sprints;
  }

  public long getfutureSprintsCount(){
    return sprints.stream().filter(s-> s.getState().equals("future")).count();
  }

  public List<Sprint> getFutureSprints() {
    List<Sprint> list = sprints.stream().filter(s -> s.getState().equals("future")).collect(Collectors.toList());
    return list.size() > 3 ? list.subList(0,2) : list;
  }

  public List<Sprint> getThreeLastSprints() {
    List<Sprint> activeSprintList = sprints.stream().filter(s -> s.getState().equals("active")).collect(Collectors.toList());
    Sprint activeSprint;
    try {
      activeSprint = activeSprintList.get(0);
    }catch (IndexOutOfBoundsException e){
      return new ArrayList<>(0);
    }
    return sprints.subList(sprints.indexOf(activeSprint) - 3 , sprints.indexOf(activeSprint));
  }
}
