package com.nix.metrics.gojira.gojirametricsdemo.entity;

import java.util.Objects;

public class TableRow {
    private Object value;
    private String fieldName;
    private String fieldDescription;
    private String numberDescription;

    public TableRow() {
    }

    public TableRow(Object value, String fieldName, String fieldDescription, String numberDescription) {
        this.value = value;
        this.fieldName = fieldName;
        this.fieldDescription = fieldDescription;
        this.numberDescription = numberDescription;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }

    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    public String getFieldDescription() {
        return fieldDescription;
    }

    public void setFieldDescription(String fieldDescription) {
        this.fieldDescription = fieldDescription;
    }

    public String getNumberDescription() {
        return numberDescription;
    }

    public void setNumberDescription(String numberDescription) {
        this.numberDescription = numberDescription;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TableRow tableRow = (TableRow) o;
        return Objects.equals(value, tableRow.value) &&
                Objects.equals(fieldName, tableRow.fieldName) &&
                Objects.equals(fieldDescription, tableRow.fieldDescription) &&
                Objects.equals(numberDescription, tableRow.numberDescription);
    }

    @Override
    public int hashCode() {

        return Objects.hash(value, fieldName, fieldDescription, numberDescription);
    }

    public static TableRow create(Object value, String fieldName, String fieldDescription, String numberDescription){
        return new TableRow(value, fieldName, fieldDescription, numberDescription);
    }
}
