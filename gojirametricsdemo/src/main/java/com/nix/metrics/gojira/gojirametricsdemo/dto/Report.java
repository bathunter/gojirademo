package com.nix.metrics.gojira.gojirametricsdemo.dto;

import com.nix.metrics.gojira.gojirametricsdemo.entity.TableRow;

import java.util.List;

public class Report {

  private TableRow velocity;
  private TableRow startDelay;
  private TableRow unestimatedIssuesPerStart;
  private TableRow issuesAddedDuringSprint;
  private TableRow issuesChangedDuringSprint;
  private TableRow midSprintProgress;
  private TableRow codeFreezeProgress;
  private List<TableRow> averageTimeInColumn;
  private List<TableRow> backFromColumn;
  private TableRow requirementsMaturityFactor;
//  private TableRow nextSprintsReadinessFactor;







//  private TableRow issuesEstimateSum;
//  private TableRow issuesInitialEstimateSum;
//  private TableRow statusTime;
//  //private long nonBugIssuesAddedDuringSprint;
//  private TableRow storyPointsChangedStat;
//  private TableRow storyPointsAddedSum;
//  private TableRow descriptionChangedStat;
//  private TableRow sprintLate;
//  private TableRow sprintMiddate;
//  private TableRow unestimatedIssuesPerEnd;
//  private TableRow addedIssueKeys;
//  private TableRow addedNOnbugIssueKeys;
//  private TableRow pointsCommitedByMidpoint;
  private String boardID;
  private String springID;

  public Report(String boardID, String springID) {
    this.boardID = boardID;
    this.springID = springID;
  }

  public TableRow getVelocity() {
    return velocity;
  }

  public void setVelocity(TableRow velocity) {
    this.velocity = velocity;
  }

  public TableRow getUnestimatedIssuesPerStart() {
    return unestimatedIssuesPerStart;
  }

  public void setUnestimatedIssuesPerStart(TableRow unestimatedIssuesPerStart) {
    this.unestimatedIssuesPerStart = unestimatedIssuesPerStart;
  }

  public TableRow getStartDelay() {
    return startDelay;
  }

  public void setStartDelay(TableRow startDelay) {
    this.startDelay = startDelay;
  }

  public TableRow getIssuesAddedDuringSprint() {
    return issuesAddedDuringSprint;
  }

  public void setIssuesAddedDuringSprint(TableRow issuesAddedDuringSprint) {
    this.issuesAddedDuringSprint = issuesAddedDuringSprint;
  }

  public TableRow getIssuesChangedDuringSprint() {
    return issuesChangedDuringSprint;
  }

  public void setIssuesChangedDuringSprint(TableRow issuesChangedDuringSprint) {
    this.issuesChangedDuringSprint = issuesChangedDuringSprint;
  }

  public TableRow getMidSprintProgress() {
    return midSprintProgress;
  }

  public void setMidSprintProgress(TableRow midSprintProgress) {
    this.midSprintProgress = midSprintProgress;
  }

  public TableRow getCodeFreezeProgress() {
    return codeFreezeProgress;
  }

  public void setCodeFreezeProgress(TableRow codeFreezeProgress) {
    this.codeFreezeProgress = codeFreezeProgress;
  }

  public List<TableRow> getAverageTimeInColumn() {
    return averageTimeInColumn;
  }

  public void setAverageTimeInColumn(List<TableRow> averageTimeInColumn) {
    this.averageTimeInColumn = averageTimeInColumn;
  }

  public List<TableRow> getBackFromColumn() {
    return backFromColumn;
  }

  public void setBackFromColumn(List<TableRow> backFromColumn) {
    this.backFromColumn = backFromColumn;
  }

  public TableRow getRequirementsMaturityFactor() {
    return requirementsMaturityFactor;
  }

  public void setRequirementsMaturityFactor(TableRow requirementsMaturityFactor) {
    this.requirementsMaturityFactor = requirementsMaturityFactor;
  }

//  public TableRow getNextSprintsReadinessFactor() {
//    return nextSprintsReadinessFactor;
//  }
//
//  public void setNextSprintsReadinessFactor(TableRow nextSprintsReadinessFactor) {
//    this.nextSprintsReadinessFactor = nextSprintsReadinessFactor;
//  }
}
