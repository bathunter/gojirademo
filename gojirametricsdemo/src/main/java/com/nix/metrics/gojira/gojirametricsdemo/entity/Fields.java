package com.nix.metrics.gojira.gojirametricsdemo.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.nix.metrics.gojira.gojirametricsdemo.deserializer.MultiDateDeserializer;
import java.util.Date;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Fields {

  @JsonProperty("assignee")
  private Person assignee;
  @JsonProperty("creator")
  private Person creator;
  @JsonProperty("reporter")
  private Person reporter;
  @JsonProperty("toString")
  private String toString;
  @JsonProperty("created") @JsonDeserialize(using = MultiDateDeserializer.class)
//  @JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd'T'HH:mm:ss.SSSz", timezone="EEST")
  private Date created;
  @JsonProperty("updated") @JsonDeserialize(using = MultiDateDeserializer.class)
//  @JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd'T'HH:mm:ss.SSSz", timezone="EEST")
  private Date updated;
  @JsonProperty("sprint")
  private Sprint sprint;
  @JsonProperty("summary")
  private String summary;
  @JsonProperty("issuetype")
  private IssueType issuetype;
  @JsonProperty("project")
  private Project project;
  @JsonProperty("status")
  private Status status;
  @JsonProperty("description")
  private String description;

  public IssueType getIssuetype() {
    return issuetype;
  }

  public Status getStatus() {
    return status;
  }

  public Date getCreated() {
    return created;
  }
}
