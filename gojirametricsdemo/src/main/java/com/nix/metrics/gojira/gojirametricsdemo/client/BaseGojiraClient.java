package com.nix.metrics.gojira.gojirametricsdemo.client;

import static com.nix.metrics.gojira.gojirametricsdemo.client.Headers.BASIC;

import java.io.IOException;
import java.net.URI;
import java.util.Base64;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.ClientRequestContext;
import javax.ws.rs.client.ClientRequestFilter;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import org.glassfish.jersey.jackson.internal.jackson.jaxrs.json.JacksonJsonProvider;

public abstract class BaseGojiraClient {

  private final String username;
  private final String appToken;
  private final String jUrl;
  private Client client;

  public BaseGojiraClient(String username, String password, String jUrl) {
    this.username = username;
    this.appToken = password;
    this.jUrl = jUrl;
 //   this.client = getClient();
  }

  protected String encodeCredentials() {
    byte[] credentials = (this.username + ':' + this.appToken).getBytes();
    return Base64.getEncoder().encodeToString(credentials);
  }


  protected URI getJiraUri() {
    return URI.create(this.jUrl);
  }


  protected Client getClient() {
    return (client == null) ?
        ClientBuilder.newClient()
            .register(JacksonJsonProvider.class)
            .register(new ClientRequestFilter() {
              @Override
              public void filter(ClientRequestContext requestContext) throws IOException {
                requestContext.getHeaders().add(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON);
                requestContext.getHeaders()
                    .add(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON);
                requestContext.getHeaders()
                    .add(HttpHeaders.AUTHORIZATION, BASIC.getHeader() + encodeCredentials());
              }
            })
        : client;

  }

}
