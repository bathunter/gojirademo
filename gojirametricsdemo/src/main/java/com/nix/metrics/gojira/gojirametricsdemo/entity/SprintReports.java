package com.nix.metrics.gojira.gojirametricsdemo.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SprintReports {
  @JsonProperty("contents")
  private SprintReport sprintReport;
  @JsonProperty("sprint")
  private Sprint sprint;


  public SprintReport getSprintReport() {
    return sprintReport;
  }

  public Sprint getSprint() {
    return sprint;
  }
}
