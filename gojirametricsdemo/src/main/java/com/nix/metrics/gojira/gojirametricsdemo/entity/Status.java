package com.nix.metrics.gojira.gojirametricsdemo.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Status {
  @JsonProperty("id")
  private int id;
  @JsonProperty("name")
  private String name;

  public String getName() {
    return name;
  }
}
