package com.nix.metrics.gojira.gojirametricsdemo.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;
import java.util.Map;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Velocities {

    @JsonProperty("velocityStatEntries")
    private Map<String, Velocity> velocities;

    public Map<String, Velocity> getVelocities() {
        return velocities;
    }
}
