package com.nix.metrics.gojira.gojirametricsdemo.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Project {
  @JsonProperty("id")
  private String id;
  @JsonProperty("key")
  private String key;
  @JsonProperty("name")
  private String name;
  @JsonProperty("projectTypeKey")
  private String projectTypeKey;
}
