package com.nix.metrics.gojira.gojirametricsdemo.client;

public enum IssueType {
  STORY("Story"),
  TASK("Task"),
  BUG("Bug");

  private String name;

  IssueType(String name) {
    this.name = name;
  }

  public String getName() {
    return name;
  }
}
