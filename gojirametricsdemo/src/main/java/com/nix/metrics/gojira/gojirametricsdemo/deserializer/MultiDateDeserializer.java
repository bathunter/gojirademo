package com.nix.metrics.gojira.gojirametricsdemo.deserializer;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.Locale;

// unashamedly stolen form https://stackoverflow.com/questions/24912992/configure-jackson-to-parse-multiple-date-formats/24986515#24986515
public class MultiDateDeserializer extends StdDeserializer<Date> {

  private static final long serialVersionUID = 1L;

  private static final String[] DATE_FORMATS = new String[] {
      "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'",
      "yyyy-MM-dd'T'HH:mm:ss.SSSz",
      "dd/MMM/yy h:mm a"
  };


  protected MultiDateDeserializer(Class<?> vc) {
    super(vc);
  }

  public MultiDateDeserializer() {
    this(null);
  }

  @Override
  public Date deserialize(JsonParser jsonParser, DeserializationContext deserializationContext)
      throws IOException, JsonProcessingException {
    JsonNode node = jsonParser.getCodec().readTree(jsonParser);
    final String date = node.textValue();
    for (String DATE_FORMAT : DATE_FORMATS) {
      try {
        return new SimpleDateFormat(DATE_FORMAT, Locale.ENGLISH).parse(date);
      } catch (ParseException e) {
        // nothing to do with
      }
    }
    throw new JsonParseException(jsonParser, "Unparseable date: \"" + date + "\". Supported formats: " + Arrays
        .toString(DATE_FORMATS));
  }
}
