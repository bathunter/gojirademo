package com.nix.metrics.gojira.gojirametricsdemo.client;

public enum Headers {

  BASIC("Basic ");

  private String header;

  Headers(String header) {
    this.header = header;
  }

  public String getHeader() {
    return header;
  }
}
