package com.nix.metrics.gojira.gojirametricsdemo.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ChangeLog {
  @JsonProperty("histories")
  private List<History> histories;

  public List<History> getHistories() {
    return histories;
  }

  public List<History> getHistories(Date startDate, Date endDate){
    return histories.stream()
        .filter(h -> h.getCreated().after(startDate) && h.getCreated().before(endDate))
        .collect(Collectors.toList());
  }

  public List<History> getHistories(Date endDate){
    return histories.stream()
            .filter(h -> h.getCreated().before(endDate))
            .collect(Collectors.toList());
  }

}
