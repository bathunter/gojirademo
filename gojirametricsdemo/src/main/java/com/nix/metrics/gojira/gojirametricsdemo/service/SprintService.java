package com.nix.metrics.gojira.gojirametricsdemo.service;

import com.nix.metrics.gojira.gojirametricsdemo.dto.Report;
import com.nix.metrics.gojira.gojirametricsdemo.entity.Board;
import com.nix.metrics.gojira.gojirametricsdemo.entity.Sprint;
import java.io.File;
import java.util.List;
import java.util.Map;

public interface SprintService {

  Report getReport(String boardID, String id, String sprintName);
  File getReport();
  List<Board> getBoards();
  List<Sprint> getSprints(String boardId);
  List<String> getSprintNames();
  Map<String, Report> generateReport(String sprintId);
}
