package com.nix.metrics.gojira.gojirametricsdemo.client;

public enum PathParts {

  ISSUE("rest/api/2/issue"),
  VELOCITY("rest/greenhopper/1.0/rapid/charts/velocity"),
  CHANGELOG("changelog"),
  SPRINT("sprint"),
  AISSUE("issue"),
  AGILEBOARD("rest/agile/1.0/board"),
  SPRINTREPORT("rest/greenhopper/latest/rapid/charts/sprintreport"),
  SEARCH("rest/api/2/search");

  private String part;

  PathParts(String part) {
    this.part = part;
  }

  public String getPart() {
    return part;
  }
}
