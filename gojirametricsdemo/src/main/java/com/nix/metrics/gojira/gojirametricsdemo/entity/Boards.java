package com.nix.metrics.gojira.gojirametricsdemo.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;


@JsonIgnoreProperties(ignoreUnknown = true)
public class Boards{
  @JsonProperty("values")
  private List<Board> boards;

  public List<Board> getBoards() {
    return boards;
  }
}
