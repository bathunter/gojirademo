package com.nix.metrics.gojira.gojirametricsdemo.utils;

import java.util.Date;

public class Interval {
  private Date startDate;

  public void setStartDate(Date startDate) {
    this.startDate = startDate;
  }

  public void setEndDate(Date endDate) {
    this.endDate = endDate;
  }

  private Date endDate;

  public Interval(Date startDate, Date endDate) {
    this.startDate = startDate;
    this.endDate = endDate;
  }

  public Date getEndDate() {
    return endDate;
  }

  public Date getStartDate() {
    return startDate;
  }

  public boolean contains(Date date){
    return date.after(startDate) && date.before(endDate);
  }
}
