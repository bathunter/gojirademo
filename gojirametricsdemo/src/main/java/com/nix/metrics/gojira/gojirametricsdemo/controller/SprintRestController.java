package com.nix.metrics.gojira.gojirametricsdemo.controller;

import com.nix.metrics.gojira.gojirametricsdemo.dto.Report;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletResponse;

import com.nix.metrics.gojira.gojirametricsdemo.entity.Board;
import com.nix.metrics.gojira.gojirametricsdemo.entity.Sprint;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.nix.metrics.gojira.gojirametricsdemo.service.SprintService;

@RestController
@RequestMapping(value = "/boards")
public class SprintRestController {

@Autowired
  private SprintService sprintService;

  @RequestMapping(path = "/sprint/{id}", method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_UTF8_VALUE })
  public Map<String, Report> getSprintById(@PathVariable(value = "id") String id) {
    return sprintService.generateReport(id);
  }
  @RequestMapping(path = "/sprint_names", method = RequestMethod.GET)
  public List<String> getSprintNames(){
      return sprintService.getSprintNames();
  }


  @RequestMapping(path = "/report", method = RequestMethod.GET, produces = { MediaType.APPLICATION_OCTET_STREAM_VALUE })
  public ResponseEntity<InputStreamResource> getReport(HttpServletResponse response) throws FileNotFoundException {
    File reportFile = null;
    try {
      reportFile = sprintService.getReport();
    //TODO Reportexception instead of Exception
    } catch (Exception e) {
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }

    HttpHeaders respHeaders = new HttpHeaders();
    respHeaders.setContentType(MediaType.APPLICATION_OCTET_STREAM);
    respHeaders.setContentDispositionFormData("attachment", "reportFile.xlsx");

    InputStreamResource isr = new InputStreamResource(new FileInputStream(reportFile));
    return new ResponseEntity<>(isr, respHeaders, HttpStatus.OK);
  }

  @RequestMapping(value = "/get", method = RequestMethod.GET)
  public List<Board> getBoards(){
    return sprintService.getBoards();
  }

  @RequestMapping(value = "/sprints/get/{boardId}", method = RequestMethod.GET)
  public List<Sprint> getSprints(@PathVariable(value = "boardId") String boardId){
    return sprintService.getSprints(boardId);
  }

}
