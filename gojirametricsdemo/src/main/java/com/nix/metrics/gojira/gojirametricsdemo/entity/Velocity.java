package com.nix.metrics.gojira.gojirametricsdemo.entity;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Map;

public class Velocity {

    @JsonProperty("estimated")
    private Map<String, Double> estimated;
    @JsonProperty("completed")
    private Map<String, Double> completed;

    public Map<String, Double> getEstimated() {
        return estimated;
    }

    public Map<String, Double> getCompleted() {
        return completed;
    }
}
