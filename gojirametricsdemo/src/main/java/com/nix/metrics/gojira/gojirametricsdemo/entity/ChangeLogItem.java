package com.nix.metrics.gojira.gojirametricsdemo.entity;

//import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.nix.metrics.gojira.gojirametricsdemo.deserializer.MultiDateDeserializer;
import java.util.Date;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ChangeLogItem {
  @JsonProperty("author/name")
  private String author;
  @JsonProperty("created") @JsonDeserialize(using = MultiDateDeserializer.class)
//  @JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd'T'HH:mm:ss.SSSz", timezone="EEST")
  private Date created;
  @JsonProperty("items")
  private List<Item> items;

  public Date getCreated() {
    return created;
  }
}
