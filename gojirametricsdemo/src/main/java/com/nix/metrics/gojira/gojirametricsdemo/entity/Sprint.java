package com.nix.metrics.gojira.gojirametricsdemo.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.nix.metrics.gojira.gojirametricsdemo.deserializer.MultiDateDeserializer;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.Period;
import java.time.ZoneId;
import java.util.Date;


@JsonIgnoreProperties(ignoreUnknown = true)
public class Sprint {

  @JsonProperty("id")
  private String id;
  @JsonProperty("name")
  private String name;
  @JsonProperty("state")
  private String state;
  @JsonProperty("startDate")
  @JsonDeserialize(using = MultiDateDeserializer.class)
//  @JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
  private Date startDate;
  @JsonProperty("endDate")
  @JsonDeserialize(using = MultiDateDeserializer.class)
//  @JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
  private Date endDate;
  @JsonIgnore
  private int sprintLate;
  @JsonIgnore
  private Date sprintMidDate;

  public Date getStartDate() {
    return startDate;
  }

  public Date getEndDate() {
    return endDate;
  }

  @JsonIgnore
  public int getSprintLate() {
    int ref = DayOfWeek.WEDNESDAY.getValue();
    int curr = getDayOfWeek(startDate).getValue();
    int week = DayOfWeek.SUNDAY.getValue();

    return (ref <= curr) ? curr - ref : curr - ref + week;
  }

  private DayOfWeek getDayOfWeek(Date date) {
    return toLocal(date).getDayOfWeek();
  }

  public String getState() {
    return state;
  }

  @JsonIgnore
  public Date getSprintMidDate() {
    return fromLocal(toLocal(startDate)
        .plusDays(Period.between(toLocal(startDate), toLocal(endDate)).getDays() / 2));
  }

  public Date getSprintMidPoint(){
    return new Date((startDate.getTime() + endDate.getTime()) / 2);
  }

  private LocalDate toLocal(Date date) {
    return date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
  }

  private Date fromLocal(LocalDate localDate) {
    return java.util.Date.from(localDate.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());
  }

  public String getName() {
    return name;
  }

  public String getId() {
    return id;
  }
}
