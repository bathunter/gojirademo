package com.nix.metrics.gojira.gojirametricsdemo.entity;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ChangeLogItems{
  @JsonProperty("values")
  private List<ChangeLogItem> changeLogItemList;

  public List<ChangeLogItem> getChangeLogItemList() {
    return changeLogItemList;
  }
}
