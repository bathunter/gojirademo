package com.nix.metrics.gojira.gojirametricsdemo.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
@JsonIgnoreProperties(ignoreUnknown = true)
public class Item{
  @JsonProperty("field")
  private String field;
  @JsonProperty("fieldId")
  private String fieldID;
  @JsonProperty("fromString")
  private String fromString;
  @JsonProperty("toString")
  private String toString;

  public String getField() {
    return field;
  }

  public String getFromString() {
    return fromString;
  }

  public String getToString() {
    return toString;
  }
}
